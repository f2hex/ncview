#!/usr/bin/env python3

import argparse
import subprocess, time, os
import yaml
import i3
from i3ipc import Connection, Event

VERSION=0.1

class Struct(object):
    def __init__(self, data):
        for name, value in data.items():
            setattr(self, name, self._wrap(value))

    def _wrap(self, value):
        if isinstance(value, (tuple, list, set, frozenset)):
            return type(value)([self._wrap(v) for v in value])
        else:
            return Struct(value) if isinstance(value, dict) else value

class NetcamView():
    def __init__(self, ncdata):
        self.netcams = ncdata.netcams
        self.nc_user = ncdata.user
        self.nc_pwd = ncdata.password
        self.player_cmd = ncdata.player
        self.ibrow_name = ncdata.ibrow_name
        self.ibrow_cmd = ncdata.ibrow_cmd
        self.img_store = ncdata.img_store
        self.netcam = {}
        self.protocol = ncdata.protocol
        self.curr_nc = 0
        self.conn = Connection()

    def next_nc(self):
        if self.curr_nc < len(self.netcams):
            self.netcam = self.netcams[self.curr_nc]
            self.curr_nc += 1
        else:
            self.netcam = None

    def start(self):
        if self.netcam:
            cmd = f"{self.player_cmd} {self.protocol}://{self.nc_user}:{self.nc_pwd}@{self.netcam.host}{self.netcam.camopt}"
            self.conn.command(f"exec {cmd}")

    def stop(self):
        if self.netcam:
            self.conn.command(f'[title="{self.netcam.name}"] focus')
            self.conn.command("kill")

    def _new_window_vwall(self, conn, event):
        subprocess.Popen(["xdotool", "search", "--name", f"{self.player_cmd}", "set_window", "--name", f"{self.netcam.name}" ])
        focus_to = self.netcam.focus
        if focus_to:
            conn.command(f'[title="{focus_to}"] focus')
        split_dir = self.netcam.split
        if split_dir:
            conn.command(f"split {split_dir}")
        self.next_nc()
        if self.netcam:
            self.start()
        else:
            self.conn.off(self._new_window_vwall)
            conn.main_quit()

    def _new_window_ibrow(self, conn, event):
        subprocess.Popen(["xdotool", "search", "--name", f"{self.ibrow_cmd}", "set_window", "--name", f"{self.ibrow_name}" ])
        #self.conn.command(f"workspace vwall")  ncdata.netcams
        time.sleep(2)
        conn.command(f'[title="{self.netcams[0].name}"] focus')
        conn.main_quit()

    def _close_window(self, conn, event):
        self.next_nc()
        if self.netcam:
            self.stop()
        else:
            conn.main_quit()

    def start_all(self):
        self.conn.command(f"workspace vwall")
        self.conn.on(Event.WINDOW_NEW, self._new_window_vwall)
        # create first window
        self.next_nc()
        self.start()
        # wait for events
        self.conn.main()

    def stop_all(self):
        self.conn.on(Event.WINDOW_CLOSE, self._close_window)
        self.next_nc()
        self.stop()
        self.conn.main()

    def focus_next(self):
        nx = 0
        next_nc = None
        ncams = len(self.netcams)
        current = i3.filter(nodes=[], focused=True)
        for ncam in self.netcams:
            nx += 1
            if ncam.name == current[0]['name']:
                if nx == ncams:
                    next_nc = self.netcams[0].name
                else:
                    next_nc = self.netcams[nx].name
        if next_nc:
            i3.focus(title=next_nc)

    def focus_prev(self):
        nx = 0
        prev_nc = None
        ncams = len(self.netcams)
        current = i3.filter(nodes=[], focused=True)
        for ncam in self.netcams:
            nx += 1
            if ncam.name == current[0]['name']:
                if nx == 1:
                    prev_nc = self.netcams[ncams-1].name
                else:
                    prev_nc = self.netcams[nx-2].name
        if prev_nc:
            i3.focus(title=prev_nc)

    def start_ibrow(self):
        self.conn.command(f"workspace snaps")
        self.conn.on(Event.WINDOW_NEW, self._new_window_ibrow)
        cmd = f"{self.ibrow_cmd} -R 4 {self.img_store}"
        self.conn.command(f"exec {cmd}")
        # wait for events
        self.conn.main()

    def fullscreen(self):
        focused = i3.filter(nodes=[], focused=True)[0]['name']
        self.conn.command(f'[title="{focused}"] fullscreen toggle')

    def shutdown(self, reboot):
        subprocess.Popen(["sudo", "shutdown", f"-h{reboot}", "now" ])

def main():
    """
    Entry point
    """
    home_dir = os.getenv("HOME")
   # Parameter check and parsing
    parser = argparse.ArgumentParser(description="Netcams Video Wall", epilog="Start netcams video wall")
    parser.add_argument("-v", "--version", action="version", version="%(prog)s {0}".format(VERSION))
    parser.add_argument("-c", "--config", action="store", dest="config", help="configuration file", required = False, default=f"{home_dir}/.ncview/netcams.yaml")
    parser.add_argument("cmd", metavar="COMMAND", type=str, help="command: can be 'start', 'stop'")
    args = parser.parse_args()

    with open(args.config) as inpf:
        config_yaml = yaml.load(inpf, Loader=yaml.SafeLoader)
    ncdata = Struct(config_yaml)

    if args.cmd:
        ncv = NetcamView(ncdata)
        if args.cmd == "start":
            ncv.start_all()
            ncv.start_ibrow()
        elif args.cmd == "stop":
            ncv.stop_all()
        elif args.cmd == "next":
            ncv.focus_next()
        elif args.cmd == "prev":
            ncv.focus_prev()
        elif args.cmd == "full":
            ncv.fullscreen()
        elif args.cmd == "shutdown":
            ncv.shutdown(reboot="")
        elif args.cmd == "reboot":
            ncv.shutdown(reboot="r")

if __name__ == '__main__':
    main()

# Netcam View

Video wall for network cameras using an ARM based board, gstreamer and i3 window manager.

## Links about hadrware and software used in this project

* [Radxa Rock pi 4 6 core ARM single board computer](https://wiki.radxa.com/Rockpi4)
* [Armbian Linux - for ARM based boards](https://armbian.com)
* [i3](https://i3wm.org/docs/userguide.html): a tiling window manager for Linux
* [gstreamer gst-play-1.0](https://gstreamer.freedesktop.org/documentation/frequently-asked-questions/using.html)
* [python i3ipc package](https://i3ipc-python.readthedocs.io/)
* [python i3 packages](https://github.com/ziberna/i3-py)

## Architecture

Quit simple, there is single program, [ncview.py](ncview.py) that perform the several tasks by chosing the specified command:

* **start**: start all the streams that must be displayed on the video wall - each stream has its own (tiled) window)
* **stop**: stop (terminate and close) all the streams that are part of the video wall
* **next**: move from the current (focused) stream window to the next one, following a left to right and top to bottom direction
* **prev**: move from the current (focused) stream window to the previous one, following a right to left and bottom to top direction
* **full**: toggle the full screen mode on the current focused stream window
* **shutdown**: perform a shutdown of the system
* **reboot**: perform a reboot of the system

The configuration file contains all the info required by `ncview.py` program.

## Configuration

The `ncview.py` program expects a specific configuration file, `${HOME}'/.ncview/netcams.yaml`, or passed explicitly using the `-c` option.
The content of the file should be like this one:

``` yaml
---
user: user
password: password
player: gst-play-1.0
protocol: rtsp
netcams:
  - name: polifemo
    host: polifemo.olimpo.lan
    focus:
    split:
    camopt: "/2"
  - name: tuke
    host: tuke.olimpo.lan
    focus:
    split:
    camopt: ''
  - name: kratos
    host: kratos.olimpo.lan
    focus: polifemo
    split: vertical
    camopt: ''
  - name: scilla
    host: scilla.olimpo.lan
    focus: tuke
    split: vertical
    camopt: ''
  - name: cariddi
    host: cariddi.olimpo.lan
    focus: kratos
    split: vertical
    camopt: ''
  - name: geryon
    host: geryon.olimpo.lan
    focus: polifemo
    split:
    camopt: ''

```

where there is a YAML defined list of netcams. The order in the list and the `focus` and `split` properties are relevant to create
the layout of the video wall - following the layout model of the `i3` window manager. In practice with the list as written abobe
after having created the third stream window the split mode is set to vertical and the focus on the first created window, in this
way the 4th window will be created by splitting the vertical space in two, this is repeated for the last two windows, in this way a
grid layout with 2 rows by 3 columns is created.

## i3 window manager configuration

In order to have the video wall started after the login the following lines can be added to the i3 configuration file:

``` config
exec ~/ncview/start_vwall
```

The content of the script `start_vwall`:

``` bash
#!/usr/bin/env bash
if [ ! -v VIRTUAL_ENV ]; then
    source ./pv/bin/activate
fi
export DISPLAY=:0.0
# disable screen saving and screen power saving
xset s off
xset -dpms
# clean up any active netcam stream play, just in case...
killall gst-play-1.0
# start netcam view wall
./ncview.py start
```



# Using a joystick to control the video-wall

The video-wall can be controlled using a joystick instead of a keyboard.

The device is composed by a joystick device and an Analog to Digital Converted that talks to the host system using I2C serial
communication channel.

## The joystick

This a cheap joystick device with a connector with 5 pin:
1. ground
2. +5V
3. VRx
4. VRy
5. switch: button at the center of the stick head

![joystick](joystick.png)

## The ADC module

The **PCF8591** is an 8 bit analog to digital or 8 bit digital to analog converter module meaning each pin can read analog values up
to 256. It also has LDR and thermistor circuit provided on the board. This module has four analog input and one analog output. It
works on [I2C
communication](https://circuitdigest.com/microcontroller-projects/arduino-i2c-tutorial-communication-between-two-arduino), so there
are SCL and SDA pins for serial clock and serial data address. It requires 2.5-6V supply voltage and have low stand-by current. We
can also manipulate the input voltage by adjusting the knob of potentiometer on the module. There are also three jumpers on the
board. J4 is connected to select the thermistor access circuit, J5 is connected to select the LDR/photo resistor access circuit and
J6 is connected to select the adjustable voltage access circuit. To access these circuits you have to use the addresses of these
jumpers: 0x50 for J6, 0x60 for J5 and 0x70 for J4. There are two LEDs on board D1 and D2- D1 shows the output voltage intensity and
D2 shows the intensity of supply voltage. Higher the output or supply voltage, higher the intensity of LED D1 or D2. You can also
test these LEDs by using a potentiometer on VCC or on AOUT pin.

![ADC Module](PCF8591-ADC-DA-Module.jpg)



## Bill of Material:

* **Joystick**: [this model](https://www.amazon.it/gp/product/B06Y1HB7KR/) available on Amazon is just enough for the need; it includes also a button, at the center of the stick, useful
  to select the item under the cursor
* **ADC module**: an 8 bit ADC, in this case a [board based on
  PCF8581](https://www.amazon.it/ZHITING-convertitore-analogico-Digitale-conversione/dp/B08GLXS5ZV/) available on Amazon used to read analog values from
  the joystick and convert them to digital number (up to 256);
* a case, to be printed from a 3D model;

## Interfacing the board

Article [here](https://circuitdigest.com/microcontroller-projects/arduino-pcf8591-adc-dac-module-interfacing).
